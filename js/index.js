(function () {
  'use strict';
  angular
      .module('MyApp',['ngMaterial', 'ngMessages', 'material.svgAssetsCache'])
      .controller('SumoCtrl', SumoCtrl);

  function SumoCtrl($mdDialog) {
    var self = this;

    self.openDialog = function($event) {
      $mdDialog.show({
        controller: DialogCtrl,
        controllerAs: 'ctrl',
        templateUrl: 'dialog.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: $event,
        clickOutsideToClose:true
      })
    }
  }

  function DialogCtrl ($timeout, $q, $scope, $mdDialog) {
    var self = this;
    self.errorMsg = "";
    self.IsTeamSelected = true;

    self.data = [
              {team: 'Engineering', employees: ['Lawana Fan', 'Larry Rainer', 'Rahul Malik', 'Leah Shumway']}, {team: 'Executive', employees: ['Rohan Gupta', 'Ronda Dean', 'Robby Maharaj']},
              {team: 'Finance', employees: ['Caleb Brown', 'Carol Smithson', 'Carl Sorensen']},
              {team: 'Sales', employees: ['Ankit Jain', 'Anjali Maulingkar']}
             ];

    self.teams = self.data.map(function(item,index){
      return item.team
    });

    self.selectedItemChange = function(item){
      if(item){
        self.IsTeamSelected = false;
        self.searchTxt = null;
        self.employees = item.employees.map(function(item,index){
          return {
              name : item
            }
        });
        // self.querySearch1("");
      }else{
        self.IsTeamSelected = true;
        self.selectedEmployee = "";
      }
    }

    function querySearch (query) {

      var results = query ? self.data.filter( createFilterFor(query) ) : self.data,
          deferred;
      if (self.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }

    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(item) {
        return (angular.lowercase(item.team).indexOf(lowercaseQuery) === 0);
      };

    }

    function querySearch1 (query) {

      var results = query ? self.employees.filter( createFilterFor1(query) ) : self.employees,
          deferred;
      if (self.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }

    function createFilterFor1(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn1(item) {
        if(item.name){
          return (angular.lowercase(item.name).indexOf(lowercaseQuery) === 0);
        }
      };

    }

    self.querySearch   = querySearch;
    self.querySearch1   = querySearch1;

    // ******************************
    // Template methods
    // ******************************

    self.cancel = function($event) {
      $mdDialog.cancel();
    };
    self.finish = function($event) {
      if(self.selectedTeam){
        if(self.selectedEmployee){
          $mdDialog.hide();
        }else{
          self.errorMsg = "* Please select Employee";
        }
      }else{
        self.errorMsg = "* Please select Team";
      }
    };
  }
})();